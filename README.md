# About this Repo #
Vive focused area for playing with and testing various VR ideas. Currently designed for HTC vive but can be forked and adapted for Rift development. 

Note: This is an experimental repo, and comes with all the risks and problems that you would usually associate with that.

Vive Teleporter script courtesy of Flafla2 (https://github.com/Flafla2/Vive-Teleporter)